#!/usr/bin/bash
#BSUB -n 12
#BSUB -n 24:00
#BSUB -R "rusage[mem=10000, scratch=10000]"
#BSUB -R "rusage[ngpus_excl_p=1] select[gpu_mtotal0>=10240]"
#BSUB -R "span[hosts=1]"
#BSUB -J alphafold

source /cluster/apps/local/env2lmod.sh
module load gcc/6.3.0 openmpi/4.0.2 alphafold/2.2.0
source /cluster/apps/nss/alphafold/venv_alphafold/bin/activate

# Define paths to databases and out put directory
DATA_DIR=/cluster/project/alphafold
OUTPUT_DIR=${TMPDIR}/output

# Activate unified memory
export TF_FORCE_UNIFIED_MEMORY=1
export XLA_PYTHON_CLIENT_MEM_FRACTION=2.0

# If use_gpu_relax is enabled, enable CUDA multi-process service. Uncomment the line below
#nvidia-cuda-mps-control -d

python /cluster/apps/nss/alphafold/alphafold-2.2.0/run_alphafold.py \
--data_dir=$DATA_DIR \
--output_dir=$OUTPUT_DIR \
--max_template_date="2022-04-04" \
--bfd_database_path=$DATA_DIR/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt \
--uniref90_database_path=$DATA_DIR/uniref90/uniref90.fasta \
--uniclust30_database_path=$DATA_DIR/uniclust30/uniclust30_2018_08/uniclust30_2018_08 \
--mgnify_database_path=$DATA_DIR/mgnify/mgy_clusters_2018_12.fa \
--template_mmcif_dir=$DATA_DIR/pdb_mmcif/mmcif_files \
--obsolete_pdbs_path=$DATA_DIR/pdb_mmcif/obsolete.dat \
--use_gpu_relax=0 \
--fasta_paths=/cluster/work/sis/cdss/jarunanp/21_12_alphafold_benchmark/scripts/alphafold_on_euler/fastafiles/IFGSC_6mer.fasta \
--model_preset=multimer --pdb_seqres_database_path=$DATA_DIR/pdb_seqres/pdb_seqres.txt --uniprot_database_path=$DATA_DIR/uniprot/uniprot.fasta \

# Disable CUDA multi-process service
#echo quit | nvidia-cuda-mps-control

mkdir -p output/IFGSC_6mer
rsync -av $TMPDIR/output/IFGSC_6mer ./output/IFGSC_6mer

